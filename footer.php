<div class="container-wrap">
  <footer id="fh5co-footer" role="contentinfo">
    <div class="row">



      <div class="col-md col-md-push-1">

          <?php wp_nav_menu(['menu' => 'footer_main', 'menu_class' => 'fh5co-footer-links']); ?>
          <?php wp_nav_menu(['menu' => 'footer_two', 'menu_class' => 'fh5co-footer2-links']); ?>
        
      </div>


    </div>

    <div class="row copyright">
      <div class="col-md-12 text-center">
        <p>
          <small class="block">&copy; 2018 Arshdeep Singh Rangi. All Rights Reserved.</small>

        </p>
      </div>
    </div>
  </footer>
</div><!-- END container-wrap -->
</div>

<div class="gototop js-top">
  <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>


<!-- Main -->
<script src="<?=get_template_directory_uri()?>/js/main.js"></script>

<?php wp_footer();?>
</body>
</html>
