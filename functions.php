<?php

if(function_exists('add_theme_support'))
{
	add_theme_support('post-thumbnails');
	add_image_size('list-thumb', 75, 75);
}

if(function_exists('register_nav_menus')){
	register_nav_menus();
}


if(!function_exists('load_arshdeep_styles'))
{
	function load_arshdeep_styles()
	{
		wp_enqueue_style('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', [], '3.3.5', 'all');
		wp_enqueue_style('assignment', get_stylesheet_uri(), [], false, 'all');
		wp_enqueue_script('modernizrjs', get_template_directory_uri().'/js/modernizr-2.6.2.min.js', [], '2.6.2', 'all');
		wp_enqueue_script('bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.bundle.min.js', [], '3.3.5', 'all');
	}

	add_action('wp_enqueue_scripts', 'load_arshdeep_styles');
}
