<?php get_header(); ?>
	<div class="container-wrap">
		<div id="fh5co-blog" class="blog-flex">
			<div class="blog-entry fh5co-light-grey" style="width:100%">
				<div class="row animate-box">
          <?php the_archive_title('<h1>','</h1>'); ?>
					<?php while(have_posts()) : ?>
						<?php the_post(); ?>
						<div class="col-md-12">
              <a href="<?php the_permalink() ?>"><?php the_title('<h2>','</h2>'); ?></a>
							<p><small>Posted on <?php the_date();?></small></p>
							<?php the_post_thumbnail('medium') ?>
							<div class="desc">
								<?php the_excerpt('<div>','</div>'); ?>
                <a href="<?php the_permalink(); ?>">READ MORE....</a>
							</div>
						</div>
					<?php endwhile; ?>
					<?php comments_template(); ?>
				</div>

			</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
