<?php get_header(); ?>
	<div class="container-wrap">
		<aside id="fh5co-hero">
			<div class="flexslider">
				<?php echo do_shortcode('[smartslider3 slider=4]');?>
		  </div>
		</aside>


		<div id="fh5co-blog" class="blog-flex">

			<?php get_sidebar(); ?>

			<div class="blog-entry fh5co-light-grey">
				<div class="row animate-box">
					<div class="col-md-12">
						<h2>Latest Posts</h2>
					</div>
				</div>

				<div id="row">
					<?php while(have_posts()) : ?>
						<?php the_post(); ?>
						<div class="col-md-12" style="margin-bottom: 30px;">
              <a href="<?php the_permalink() ?>"><?php the_title('<h2>','</h2>'); ?></a>
							<p><small>Posted on <?php the_date();?></small></p>
							<?php the_post_thumbnail('medium') ?>
							<div class="desc">
								<?php the_excerpt('<div>','</div>'); ?>
                <a href="<?php the_permalink(); ?>">READ MORE....</a>
								<hr />
							</div>
						</div>
					<?php endwhile; ?>
				</div>

			</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
