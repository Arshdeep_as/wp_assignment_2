<?php get_header(); ?>
	<div class="container-wrap">
		<div id="fh5co-blog" class="blog-flex">
			<div class="blog-entry fh5co-light-grey" style="width:100%">
				<div class="row animate-box">
          <h1>You have searched for <strong style="color: red"><?php echo get_search_query(); ?></strong></h1>
					<?php while(have_posts()) : ?>
						<?php the_post(); ?>
						<div class="col-md-12">
							<?php the_title('<h1>','</h1>'); ?>
							<p><small>Posted on <?php the_date();?></small></p>

							<div class="desc">
								<div class="featured-image">
									<?php the_post_thumbnail('medium') ?>
								</div>
								<?php the_content('<div>','</div>'); ?>
							</div>
						</div>
					<?php endwhile; ?>

				</div>

			</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
