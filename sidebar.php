<div class="featured-blog" style="background-image: url(<?=get_template_directory_uri()?>/images/blog-1.jpg);">
  <div class="desc-t">
    <div class="desc-tc">
      <span class="featured-head">Archives</span>
      <ul>
        <?php wp_get_archives() ?>
      </ul>
      <span class="featured-head">Categories</span>
      <ul>
          <?php wp_list_categories('title_li') ?>
      </ul>

    </div>
  </div>
</div>
