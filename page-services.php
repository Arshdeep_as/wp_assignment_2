<?php get_header(); ?>

  <div class="container-wrap">
    <div id="fh5co-work">
			<div class="row">
				<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(<?=get_template_directory_uri()?>/images/portfolio-1.jpg);">
						<div class="desc">
							<h3>Online Learning</h3>
						</div>
					</a>
				</div>
				<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(<?=get_template_directory_uri()?>/images/portfolio-2.jpg);">
						<div class="desc">
							<h3>Web Development</h3>
						</div>
					</a>
				</div>
				<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(<?=get_template_directory_uri()?>/images/portfolio-3.jpg);">
						<div class="desc">
							<h3>App Development</h3>
						</div>
					</a>
				</div>
				<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(<?=get_template_directory_uri()?>/images/portfolio-4.jpg);">
						<div class="desc">
							<h3>Project Name</h3>
						</div>
					</a>
				</div>
				<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(<?=get_template_directory_uri()?>/images/portfolio-5.jpg);">
						<div class="desc">
							<h3>Theme Development</h3>
						</div>
					</a>
				</div>

			</div>
		</div>
  </div><!-- END container-wrap -->

<?php get_footer(); ?>
