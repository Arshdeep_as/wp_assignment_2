<?php get_header(); ?>
	<div class="container-wrap">
		<div class="flexslider">
			<?php echo do_shortcode('[smartslider3 slider=3]'); ?>
		</div>

		<div id="fh5co-blog" class="blog-flex">
			<div class="featured-blog" style="background-image: url(<?=get_template_directory_uri()?>/images/blog-1.jpg);">
				<div class="desc-t">
					<div class="desc-tc">
						<span class="featured-head">Featured Posts</span>
						<h3><a href="#">Top 20 Best WordPress Themes 2017 Multi Purpose and Creative Websites</a></h3>
						<span><a href="#" class="read-button">Learn More</a></span>
					</div>
				</div>
			</div>
			<div class="blog-entry fh5co-light-grey">
				<div class="row animate-box">
					<div class="col-md-12">
						<h2>Latest Posts</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 animate-box">
						<a href="#" class="blog-post">
							<span class="img" style="background-image: url(<?=get_template_directory_uri()?>/images/blog-2.jpg);"></span>
							<div class="desc">
								<h3>26 Best Education WordPress Themes 2017 You Need To See</h3>
								<span class="cat">Collection</span>
							</div>
						</a>
					</div>
					<div class="col-md-12 animate-box">
						<a href="#" class="blog-post">
							<span class="img" style="background-image: url(<?=get_template_directory_uri()?>/images/blog-1.jpg);"></span>
							<div class="desc">
								<h3>16 Outstanding Photography WordPress Themes You Must See</h3>
								<span class="cat">Collection</span>
							</div>
						</a>
					</div>
					<div class="col-md-12 animate-box">
						<a href="#" class="blog-post">
							<span class="img" style="background-image: url(<?=get_template_directory_uri()?>/images/blog-3.jpg);"></span>
							<div class="desc">
								<h3>16 Outstanding Photography WordPress Themes You Must See</h3>
								<span class="cat">Collection</span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
