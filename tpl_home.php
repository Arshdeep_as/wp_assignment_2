<?php
/*
Template Name: home
*/
get_header(); ?>
	<div class="container-wrap">
		<div class="flexslider">
			<?php echo do_shortcode('[smartslider3 slider=3]'); ?>
		</div>

    <div id="fh5co-services">
			<div class="row">
        <?php get_sidebar('home'); ?>

			</div>
		</div>

		<div id="fh5co-search">
			<div class="row">
      	<?php echo get_search_form(); ?>

			</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
