<?php get_header(); ?>
	<div class="container-wrap">
		<div id="fh5co-blog" class="blog-flex">

      <div class="featured-blog" style="background-image: url(<?=get_template_directory_uri()?>/images/blog-1.jpg);">
        <div class="desc-t">
          <div class="desc-tc">
            <span class="featured-head">Category: <?php single_cat_title(); ?></span>
            <h4><?php wp_list_categories('title_li') ?></h4>
          </div>
        </div>
      </div>
      <div class="blog-entry fh5co-light-grey">
				<div class="row animate-box">
					<div class="col-md-12">
						<h2>All Posts related to this category</h2>
						<hr />
					</div>
				</div>

				<div id="row">
          <?php while(have_posts()) : ?>
						<?php the_post(); ?>
						<div class="col-md-12" style="margin-bottom: 30px;">
              <a href="<?php the_permalink() ?>"><?php the_title('<h2>','</h2>'); ?></a>
							<p><small>Posted on <?php the_date();?></small></p>
							<?php the_post_thumbnail('medium') ?>
							<div class="desc">
								<?php the_excerpt('<div>','</div>'); ?>
                <a href="<?php the_permalink(); ?>">READ MORE....</a>
								<hr />
							</div>
						</div>
					<?php endwhile; ?>
				</div>

				<div>
					
				</div>

			</div>
		</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
