<?php get_header(); ?>
	<div class="container-wrap">
		<div id="fh5co-blog" class="blog-flex">
			<div class="blog-entry fh5co-light-grey" style="width:100%">
				<div class="row animate-box">
					<h1>Error 404 Page Not Found</h1>
          <h2>The Page You requested is not available</h2>
				</div>

			</div>
		</div>
	</div><!-- END container-wrap -->

	<?php get_footer(); ?>
