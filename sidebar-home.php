<h1 style="text-align:center;">Categories</h1>
<hr />
<?php
  $taxonomy = 'category';
  $categories = get_terms($taxonomy);
?>
<?php foreach($categories as $category) :?>
  <div class="col-md-4 text-center animate-box">
    <div class="services">
      <div class="desc">
        <h3><a href="/<?=$category->slug?>"><?=$category->name?></a></h3>
        <p><?=$category->description?></p>
      </div>
    </div>
  </div>
<?php endforeach; ?>
